/*#############################################################################
 *###
 *###   Code for CE related to PRBS
 *###   ===========
 *#############################################################################
 *
 * Code belongs to TI4 CE; Informatik; HAW-Hamburg; Berliner Tor 7; D-20099 Hamburg
 *
 *
 *-----------------------------------------------------------------------------
 * Description:
 * ============
 * Support for generation and checking of 16 bit PRBS pattern.
 * Code is under development - code is still in "Schmuddel"-state.
 * ...
 *
 * ATTENTION: Writing one PRBS sequence into another destroys the other sequence!
 *
 *
 * History:
 * ========
 *   110513: 1st version by Michael Sch�fers (email: CE@Hamburg-UAS.eu)
 *   110519: 2nd release by Michael Sch�fers (email: CE@Hamburg-UAS.eu)
 *   110523: functions renamed
 *              getSingleByteOutOfPRBSstream() -> getNextByteOutOfPRBSstream()
 *              chkSingleByteOutOfPRBSstream() -> chkNextByteOutOfPRBSstream()
 *           by Michael Sch�fers (email: CE@Hamburg-UAS.eu)
 *   120515: minor modifications
 *             initPRBSstreamChecking() touched
 *             zero count introduced
 *           by Michael Sch�fers (email: CE@Hamburg-UAS.eu)
 *-----------------------------------------------------------------------------
 */


#ifndef __PRBS_based_stream_checking_UTILity_INCLUDED
//
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
#define __PRBS_based_stream_checking_UTILity_INCLUDED



//std.lib.h required for "abort()"
#include <stdlib.h>



// define prime polynomials
#define   PRIME_POLY4ARRAY    ( 0x13801 )
#define   PRIME_POLY4STREAM   ( 0x14109 )


// define trigger for zero count - a "valid sequence" must not contain more than two succeeding zeros
// ZERO_CNT_TRIGGER is used in  initPRBSstreamChecking()
#define   ZERO_CNT_TRIGGER    ( 16 )



// check return code of checkArray() of chkSingleByteOutOfPRBSstream()
#define chk_rc( rc )                                    \
    do {                                                \
        if ( rc<0 ){                                    \
            fprintf(                                    \
                stderr,                                 \
                "ERROR   rc=%d   at \"%s\" line:%d\n",  \
                rc,  __FILE__,  __LINE__                \
            );                                          \
            abort();                                    \
        }                                               \
    }while(0)







// fill array with PRBS pattern
void fillArray( uint8_t* array,  uint16_t nob ){                               // buffer,  Number Of Bytes to fill into buffer
    static uint32_t  prbsSignature  =  0x1;                                    // current PRBS signature for array related operations
    
    const uint16_t  max  =  nob & ~0x01;                                       // aligned MAXimum
    uint16_t  i;                                                               // just a working index
    
    
    i=0;
    while( i < max ){
        if ( prbsSignature & 0x01)  prbsSignature ^= PRIME_POLY4ARRAY;
        prbsSignature >>= 1;
        //
        array[i++]  =  prbsSignature & 0xff;
        array[i++]  =  prbsSignature >> 8;
    }//for
    if ( nob & 0x01 ){
        if ( prbsSignature & 0x01)  prbsSignature ^= PRIME_POLY4ARRAY;
        prbsSignature >>= 1;
        //
        array[nob] = prbsSignature & 0xff;
    }//if
}//fillArray()



// internal support function for checkArray
int32_t _subCheckArray( uint8_t* array,  uint16_t nob ){                       // buffer,  Number Of Bytes inside buffer
    uint32_t  magic;
    const uint16_t  max   = nob & ~0x01;
    uint16_t  i;
    
    
    magic = array[0] | (array[1]<<8);
    i=2;
    while( i < max ){
        if ( magic&0x01)  magic ^= PRIME_POLY4ARRAY;
        magic >>= 1;
        //
        if ( magic  !=  (array[i] | (array[i+1]<<8)) )  return -i;
        i+=2;
    }//while
    if ( nob & 0x01 ){
        if ( magic&0x01)  magic ^= PRIME_POLY4ARRAY;
        magic >>= 1;
        //
        if ( (magic>>8) != array[nob] )  return -nob;
    }//if
    return nob-2;
}//_subCheckArray()
//
// Check array filled with PRBS pattern.
// Array has to have at least size of 4 bytes.
// Negative return code means error
//   - either array was to small (less than 4 bytes)
//   - or array was faulty (contained other bytes than expected)
// Positive return code means success.
//   returned number is number of successful checked bytes
//   returned number is smaller than "nob" as result of checking algorithm
int32_t checkArray( uint8_t* array,  uint16_t nob ){                           // buffer,  Number Of Bytes inside buffer
    int32_t   succ;
    int32_t   tmp;
    
    
    succ = -1;
    if ( nob >= 4 ){
        succ = _subCheckArray( array, nob );
        if ( succ < 0 ){
            tmp = _subCheckArray( array+1, nob-1 );
            if ( tmp>0 || tmp<succ )  succ = tmp;
        }//if
    }//if
    return succ;
}//checkArray()





// get single byte out of prbs stream
// note: one 16 bit prbs word is generated for two bytes
uint8_t getNextByteOutOfPRBSstream(){
    static  uint32_t  prbsSignature = 0x1;                                     // current PRBS signature for stream related operations
    static  int8_t    byte1st = -1;                                            // true, since next byte is first byte
    
    uint8_t  tmp;
    
    
    if ( byte1st ){
        byte1st = 0x00;                                                        // false, since next byte is second byte
        return  prbsSignature & 0xff;
    }else{
        tmp = prbsSignature>>8;
        
        if ( prbsSignature & 0x01)  prbsSignature ^= PRIME_POLY4STREAM;
        prbsSignature >>= 1;
        
        byte1st = -1;                                                          // true, since next byte is first byte again
        return tmp;
    }//if
}//getSingleByteOutOfPRBSstream()



// check single byte out of prbs stream
// ATTENTION: function expects single byte(!)  if you hand over some other data (type)  it will NOT work properly
// note: two stream bytes shall equal one 16 bit prbs word
int32_t chkNextByteOutOfPRBSstream( uint8_t data ){
    static  uint32_t  magic1   = 0x00;                                         // "signature" aligned to "1st byte"
    static  uint32_t  magic2   = 0x00;                                         // "signature" aligned to "2nd byte"
    
    static  int32_t   resu     = 0;                                            // result, initialized with any value
    
    static  int8_t    zeroCnt  = 0;                                            // ZERO CouNT
    static  int8_t    byte1st  = 0;                                            // marker, initialized with any value
    static  int8_t    state    = 0;                                            // start state
    
    
    
    switch ( state ){
        
        //
        // searching for alignment
        //
        
        // 1st setup step
        case 0:
            magic1  =  data;                      // setup magic1
          //magic2  =  0x00;
            resu    =  0;                         // no error yet, but no success also
            state   =  1;
            //
            zeroCnt = (data==0) ? 1 : 0;
        break;
        
        
        
        // 2nd setup step
        case 1:
            magic1 |=  data<<8;                   // setup magic1
            magic2  =  data;                      // setup magic2
          //resu    =  0;                         // no error yet, but no success also
            state   =  2;
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        
        // 3rd setup step and 1st check on magic1
        case 2:
            // handle magic2
            //
            magic2 |= data<<8;                    // setup magic2
            
            
            // handle magic1
            //
            // compute next magic (resp. current signature)
            if ( magic1&0x01)  magic1 ^= PRIME_POLY4STREAM;
            magic1 >>= 1;
            
            // check data
            if ( (magic1&0xFF) == data ){
                resu++;                         // successfull check (resp. "resu=1")
                state    =  3;                  // still magic2 has to be checked - might be spurious alignment
            }else{
              //resu     =  0;                  // no error yet (on magic2), but no success also
                state    =  12;                 // final alignment to magic2 (still possible - but unproved)
                byte1st  =  -1;                 // next time 1st byte for magic2
            }//if
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        
        //---------------------------------------------------------------------
        //
        // at least one alignment found, but spurious alignments are still possible
        //
        
        case 3:
            // handle magic1
            //
            // check data
            if ( (magic1>>8) == data ){
                resu++;                         // successfull check
                state    =  4;
            }else{
                // do NOT touch "resu"          -- it was computed before
                state    =  12;                 // final alignment to magic2 ( if succeeding check on magic2 successful ;-)
                byte1st  =  0x00;               // next time 2nd byte for magic2
            }//if
            
            
            // handle magic2
            //
            // compute next magic (resp. current signature)
            if ( magic2&0x01)  magic2 ^= PRIME_POLY4STREAM;
            magic2 >>= 1;
            
            // check data
            if ( (magic2&0xFF) != data ){
                if ( state == 4 ){
                    // do NOT touch "resu"      -- it was computed before
                    state    =  11;             // final alignment to magic1
                    byte1st  =  -1;             // next time 1st byte for magic1
                }else{
                    resu     =  -resu;          // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users will/had stop(ped) checking
                }//if
          //}else{
          //    do NOT touch "resu"             -- it was computed before
          //    do NOT touch "state"            -- it was computed before
          //    do NOT touch "byte1st"          -- it was computed before - if needed
            }//if
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        
        case 4:
            // handle magic1
            //
            // compute next magic (resp. current signature)
            if ( magic1&0x01)  magic1 ^= PRIME_POLY4STREAM;
            magic1 >>= 1;
            
            // check data
            if ( (magic1&0xFF) == data ){
                resu++;                         // successfull check
                state    =  3;
            }else{
                // do NOT touch "resu"        - it was computed before
                state    =  12;                 // final alignment to magic2  ( if succeeding check on magic2 successful ;-)
                byte1st  =  -1;                 // next time 1st byte for magic2
            }//if
            
            
            // handle magic2
            //
            // check data
            if ( (magic2>>8) != data ){
                if ( state == 3 ){
                    // do NOT touch "resu"      -- it was computed before
                    state    =  11;             // final alignment to magic1
                    byte1st  =  0x00;           // next time 2nd byte for magic1
                }else{
                    resu     =  -resu;          // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users had stopped checking
                }//if
          //}else{
          //    do NOT touch "resu"             -- it was computed before
          //    do NOT touch "state"            -- it was computed before
          //    do NOT touch "byte1st"          -- it was computed before - if needed
            }//if
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        
        //---------------------------------------------------------------------
        //
        // final alignment found and checking
        //
        
        case 11:
            // aligned to magic1 (resp. on 1st byte)
            if ( byte1st ){
                
                // compute next magic (resp. current signature)
                if ( magic1 & 0x01)  magic1 ^= PRIME_POLY4STREAM;
                magic1 >>= 1;
                
                // check data
                if ( (magic1&0xFF) == data ){
                    resu++;                     // successfull check
                    // keep state
                    byte1st  =  0x00;           // next time 2nd byte - toggle byte1st
                }else{
                    resu     =  -resu;          // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users had stopped checking
                }//if
                
            }else{
                // check data
                if ( (magic1>>8) == data ){
                    resu++;                     // successfull check
                    // keep state
                    byte1st  =  -1;             // next time 1st byte
                }else{
                    resu     =  -resu;          // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users had stopped checking
                }//if
            }//if
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        case 12:
            // aligned to magic2 (resp. on 2nd byte)
            if ( byte1st ){
                
                // compute next magic (resp. current signature)
                if ( magic2 & 0x01)  magic2 ^= PRIME_POLY4STREAM;
                magic2 >>= 1;
                
                // check data
                if ( (magic2&0xFF) == data ){
                    resu++;                     // successfull check
                  //state = 12;                 // keep state
                    byte1st = 0x00;             // next time 2nd byte - toggle byte1st
                }else{
                    resu = -resu;               // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users had stopped checking
                }//if
                
            }else{
                // check data
                if ( (magic2>>8) == data ){
                    resu++;                     // successfull check
                  //state = 12;                 // keep state
                    byte1st = -1;               // next time 1st byte - toggle byte1st
                }else{
                    resu     =  -resu;          // mark error
                    state    =  0;              // "reset" as result of ERROR - it is expected that users had stopped checking
                }//if
            }//if
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
        
        //---------------------------------------------------------------------
        //
        // handle unexpected states
        //
        
        default:
            state  =  0;
            resu   =  -1;
            //
            zeroCnt = (data==0) ? zeroCnt+1 : 0;
        break;
        
    }//switch
    
    // check number of zero bytes
    // note: ZERO_CNT_TRIGGER is used to reset PRBS stream checking in "initPRBSstreamChecking()" too
    if ( zeroCnt >= ZERO_CNT_TRIGGER ){
        // far too many zero bytes found - it is assumed that prbs sequence was generated with counterpart function "getSingleByteOutOfPRBSstream()"
        fprintf( stderr, "ERROR   %d succeeding zeros found in data sequence  -  this stream does NOT contain a valid PRBS sequence;   state:%d, resu:%d   at \"%s\" line:%d \n",   zeroCnt, state, resu, __FILE__, __LINE__ );
        abort();
    }//if
    return resu;
}//chkNextByteOutOfPRBSstream()



void initPRBSstreamChecking(){
    int32_t i;
    
    for (i=ZERO_CNT_TRIGGER; --i>0;)  chkNextByteOutOfPRBSstream( 0x00 );      // align to "zero-cycle" (by giving ZERO_CNT_TRIGGER-1 zeros)
    chkNextByteOutOfPRBSstream( 0xFF );                                        // break "zero-cycle"
}//initPRBSstreamChecking()



//#############################################################################
//
// Anwendungsbeispiel
// ==================
//
//  ...
//  #include "prbsutil.h"
//  ...
//  #define NumberOfBytes ...
//  ...
//
//  ...
//  // writing test sequence
//  for ( i=0; i<NumberOfBytes; i++ ){
//      txByte = getNextByteOutOfPRBSstream();
//      _schreibe_jeweils_Byte_  txByte  _weg_-_z.B._ueber_SPI_
//  }
//  ...
//  //initPRBSstreamChecking();          // nur bei wiederholter Anwendung erforderlich
//  ...
//  // reading test sequence back
//  i=0;
//  do {
//      _lese_jeweils_Byte_  rxByte  _zurueck_-_z.B._ueber_SPI_
//      rc = chkNextByteOutOfPRBSstream( rxByte );  chk_rc( rc );
//  }while( i<NumberOfBytes && rc>=0 );
//  printf( "rc = %d,  i = %d\n",  rc, i );
//  ...


//
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
#endif
