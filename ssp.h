#ifndef _SSP_INCLUDE_H
#define _SSP_INCLUDE_H
#include <stdio.h>
#include "lpc24xx.h"
#include "config.h"
#include "ConfigStick.h"
#include "portlcd.h"
#include "fio.h"
void read_device_id();
    



__inline void ssp_send_dummy_bytes(unsigned int nbytes);
__inline void ssp_read_dummy_bytes(unsigned int nbytes);
__inline void start_operation(char opcode);
__inline void send_address(unsigned int addresse);
__inline void end_operation();
__inline void spi_write_enable();
__inline void spi_global_unprotect();
void spi_read_data(int addr, int nbytes, unsigned char *data);
void spi_write_data(int addr, int nbytes, unsigned char *data);
void spi_erase_block(int addr, int block_size_select);
void spi_erase_all();
void spi_wait_ready();
void spi_erase_consecutive_blocks(int addr, int block_size_select, int nblocks);
void spi_erase_bytes(int addr, int bytes);
void spi_write_bytes(int addr, int nbytes, unsigned char *data) ;
void spi_erase_all();
void spi_wait_ready();

#define OPCODE_DEV_ID 0x9F
#define OPCODE_READ 0x1B
#define OPCODE_WRITE_STATUS_REG1 0x01
#define OPCODE_WRITE 0x02
#define OPCODE_ERASE4 0x20
#define OPCODE_ERASE32 0x52
#define OPCODE_ERASE64 0xD8
#define OPCODE_CHIP_ERASE 0x60
#define STATUS_READY 0x01
#define OPCODE_READ_STATUS_REG 0x05
#define OPCODE_WRITE_ENABLE 0x06
// simplified access to SSP0 SR bits             -> see UM Chapter 20-6.4, Table 474
//
// Receive fifo Not Empty ;      SSP0-SR: RNE bit equals 0 if fifo empty
#define  SSP0_SR_RNE             ( (SSP0_SR & 0x04)  != 0 )
//
// Transmit fifo Not Empty ;     SSP0-SR: TFE bit equals 1 if fifo empty ;      fifo seems to have size of 12 bytes
#define  SSP0_SR_TNE             ( (SSP0_SR & 0x01)  != 1 )
// TRANSMIT fifo not full
#define SSP0_SR_TNF 			 ( (SSP0_SR & 0x02)  != 0 ) 

#define WAIT_FOR_FIFO_RNE while( !SSP0_SR_RNE ) {}
#define WAIT_FOR_FIFO_TNF while( !SSP0_SR_TNF ) {}

#define B64K_BYTES               (1<<16)
#define B32K_BYTES               (1<<15)
#define B4K_BYTES                (1<<12) 



#endif
