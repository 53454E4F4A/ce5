#include <stdio.h>
#include "lpc24xx.h"
#include "config.h"
#include "ConfigStick.h"
#include "portlcd.h"
#include "fio.h"
#include "prbsutil.h"
#include "ssp.h"
#include <string.h>
/*
*
*	to do:
*	implement write for any size without loss;
*	implement erase in byte without loss;
*   poll status registers;
*	reduce write calls;
*	
*/




// macro converts binary value (containing up to 8 bits resp. <=0xFF) to unsigned decimal value
// as substitute for missing 0b prefix for binary coded numeric literals.
// macro does NOT check for an invalid argument at all.
#define b(n) (                                               \
    (unsigned char)(                                         \
            ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
        |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
        |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)




// simplified acces to switches S0 - S7
#define  S0   ( (GPIO1_IOPIN & (1<< 8))  == 0 )
#define  S1   ( (GPIO1_IOPIN & (1<< 9))  == 0 )
#define  S2   ( (GPIO1_IOPIN & (1<<10))  == 0 )
#define  S3   ( (GPIO1_IOPIN & (1<<11))  == 0 )
#define  S4   ( (GPIO1_IOPIN & (1<<12))  == 0 )
#define  S5   ( (GPIO1_IOPIN & (1<<13))  == 0 )
#define  S6   ( (GPIO1_IOPIN & (1<<14))  == 0 )
#define  S7   ( (GPIO1_IOPIN & (1<<15))  == 0 )


void init() {
	GPIO0_IODIR |= (b(1)<<16);                            // GPIO Port0[pin16] has to be output
    GPIO0_IOSET  = (b(1)<<16);                            // deselect slave/flash ;  connected flash_CS is low active
    
    // Power CONtrol for Peripherals register                                                                                   -> UM Chapter 4-3.4.9, Table 63
    PCONP |= (b(1)<21);                                   // turn SSP0 on
    //
    // Peripheral CLocK SELection register 1                                                                                    -> UM Chapter 4-3.3.4, Table 57 & 58
    PCLKSEL1 = ( PCLKSEL1 & ~(b(11)<<10)) | (b(01)<<10);  // peripheral clock for SSP0 = 48MHz = Cpu CLocK
    //
    // PIN function SELect register 0                                                                                           -> UM Chapter 9-5.1,   Table 130
    PINSEL0 = ( PINSEL0 & ~(b(11)<<30)) | (b(10)<<30);    // Spi ClocK ssp0 (SCK0)
    // PIN function SELect register 1                                                                                           -> UM Chapter 9-5.2,   Table 131
    PINSEL1 =    ( PINSEL1 & ~(b(111111)<<0))
               | (  b(00)<<0)                             // ssp0 Slave SELection (SSEL) is done (manually) via related GPIO register over Port0[pin16]
               | (  b(10)<<2)                             // MISO_ssp0  (over Port0[pin17])
               | (  b(10)<<4);                            // MOSI_ssp0  (over Port0[pin18])
    
    // SSPn Control Register 0 -> UM Chapter 20-6.1, Table 471
    SSP0_CR0 =   (b(0111)<<0)                             // 8 bits/frame ;  set DSS  (Data Size Select)
               | (  b(00)<<4)                             // SPI ;           set FRF  (FRame Format)
               | (   b(1)<<6)                             // CPOL ;          set CPOL (Clock out POLarity)
               | (   b(1)<<7)                             // CPHA ;          set CPHA (Clock out PHAse)
               | (   0x00<<8);                            //                 set SCR  (Serial Clock Rate) ;   <->  SSP0_CPSR (CPSDVSR)
    // SSPn Control Register 0 -> UM Chapter 20-6.2, Table 472
    SSP0_CR1 =   (   b(0)<<0)                             // normal operation ;    set LBM  (Loop Back Mode)
               | (   b(1)<<1)                             // enable SSP0 ;         set SSPE (Synchronous Serial Port controller Enable)
               | (   b(0)<<2)                             // SSP0 is bus master ;  set MS   (Master/Slave mode)
               | (   b(0)<<3);                            // not relevant              SOD  (Slave Output Disable)
    // SSPn Clock Prescale Register (CPSDVSR) -> UM Chapter 20-6.5, Table 475
    SSP0_CPSR = 20;                 
	    // Synchronous Serial Port (SSP) -> see UM Chapter 20: LPC24XX SSP interface SSP0/1
   

                         // 48MHz/20 = 2400KHz - well suited for oscilloscope
    
}



#define BUF_SIZE 1024


int main( void ){
    
    unsigned int rxDat;  // Received DATa
    unsigned int id;     // ID of AT25DF641
    int i;   
	int j;            // just a temporary variable/index
    int operation = 0;
	unsigned char buf[BUF_SIZE];
    //memset(buf, 0, BUF_SIZE);
    unsigned char txByte = 0;
	int rc = 0;
    int start = 0;
	int length = 0;
	int count = 0;
	 char val[BUF_SIZE];
	 char realval[BUF_SIZE];
	 char *pval = val;
	int tfsblocks = 0;
	int rest = 0;

    // general setup
    // =============
    //
    BaseStickConfig();
    //
#ifdef LCD_SUPPORT     
    LCD_puts( "A5 for CEWS13" );
#endif
    
    init();
    
    

	//MAIN LOOP
    while (1) {

		printf("1: Read Vendor/Device ID\n"
			"2: Read data\n"
			"3: Erase block\n"
			"4: Write data\n"
			"5: Write PRBS\n"
			"6: Check PRBS\n");
	    operation = 0;
		memset(val, 0, sizeof(val));
		memset(buf, 0, sizeof(buf));
		scanf("%d", &operation);
		switch (operation) {
		case 1:  											// READ ID 
			printf("Reading Vendor/Device ID\n");
			read_device_id();
			break;
		case 2:											 	// READ DATA
			printf("Reading data\n");
			printf("Start Address (max 0x7FFFFF): 0x");
			scanf("%x", &start);
			printf("length in bytes: d");
			scanf("%d", &length);

			printf("start: 0x%x length: %d\n", start, length);
			spi_read_data(start, length, buf);
			for(i = 0; i < length; i++) {
				printf("0x%hhX ", buf[i]);
			}
			printf("\n");
			break;
		case 3:											 	// ERASE BLOCK
			printf("erasing block\n");
			printf("Start Address (max 0x7FFFFF): 0x");
			scanf("%x", &start);
			do {
				printf("block size:\n");
				printf("1 - 4Kb\n");
				printf("2 - 32Kb\n");
				printf("3 - 64Kb\n");
				scanf("%d", &length);
			} while (length < 1 || length > 3);
			printf("start: 0x%x length: %d\n", start, length);
			spi_erase_block(start, length);
			spi_wait_ready();	
			
			break;
		case 4:											 	//W WRITE DATA
			printf("writing data\n");
			printf("Start Address (max 0x7FFFFF): 0x");
			scanf("%x", &start);

			printf("input data: (0-f)\n");
			fflush(stdin);
			fgets(val, sizeof(val), stdin);
			memset(val, 0, sizeof(val));
			memset(buf, 0, sizeof(buf));
			fgets(val, sizeof(val), stdin);
		
			while (*pval != '\0' && *pval != '\n') {
			sscanf(pval, "%2hhx", &buf[count]);
			pval = pval + 2;
			count++;
			}	
			printf("start: 0x%x nbytes: %d\n", start, count);
			for(i = 0; i < count; i++) {
				printf("0x%hhX ", buf[i]);
			}	
			printf("\n");
			spi_write_data(start, count, buf);
 			spi_wait_ready();	
			break;
		case 5: 											 // WRITE PRBS

			printf("writing PRBS\n");
			printf("Start Address (max 0x7FFFFF): 0x");
			scanf("%x", &start);
			printf("length in bytes (decimal): ");
			scanf("%d", &length);
 			if((0x7FFFFF+1)-(start+length) < 0) {
				length = (0x7FFFFF-start)+1;
				//printf("\n\ntrying to write out of bounds\nwriting only 0x%x bytes\n\n", length);
				
			} 	
			printf("start: 0x%x length: %d\n", start, length);
 			 spi_erase_bytes(start, length);	
			 
			 tfsblocks = length / 256;
			 rest = length % 256;
			 printf("256 blocks: %d, rest: %d\n", tfsblocks, rest);
			 for(j= 0; j<tfsblocks; j++) {
			  for ( i=0; i<256; i++ ){
					buf[i] = getNextByteOutOfPRBSstream();
					//printf("getting byte %d\n", i);
			  }
			 
			  spi_write_bytes(start, 256, buf);
 			  spi_wait_ready();
			  start += 256;
			  //printf("doen block %d\n", j);
			  } 
			  
			  for(i = 0; i < rest; i++) {
				buf[i] = getNextByteOutOfPRBSstream();
			  }
			  spi_write_bytes(start, rest, buf);
 			  spi_wait_ready();
			  
			
			break;
			
		case 6:												// CHECK PRBS
			printf("checking PRBS\n");
			printf("Start Address (max 0x7FFFFF): 0x");
			scanf("%x", &start);
			printf("length in bytes: d");
			scanf("%d", &length);
			printf("start: 0x%x length: %d\n", start, length);
			initPRBSstreamChecking();  
			i=0;
		    do {
				spi_read_data(start+i, 1, &buf[0]);				
				rc = chkNextByteOutOfPRBSstream( buf[0] ); chk_rc( rc );
				
				i++;
		    } while( (i < length) && (rc >= 0) );
			printf( "rc = %d,  i = %d\n",  rc, i );
		break;				
			default:											// 0x90
 			fflush(stdin);
			printf("invalid operation\n");
			break;
		}
		
	}
    
    

    
    printf( "\nTHE END\n" );
    // NEVER stop doing task
    while (1){
    }//while
    //
    //------------------------------------------------------------------------
    // it is NOT expected that program execution passes here
    // anyway,
    
    // clean up
    // nothing to do
    
    // GUARD CODE  -  NEVER STOP !!!  -  THERE IS NO REST
    while(1);
}//main
