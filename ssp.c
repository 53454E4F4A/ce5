#include "ssp.h"

void read_device_id() {
	
	unsigned int rxDat, id;
	int i;
	start_operation(OPCODE_DEV_ID);
	ssp_send_dummy_bytes(4);
  
    rxDat=0;
    for( i=0; i<4; i++ ){
        WAIT_FOR_FIFO_RNE
        rxDat = (rxDat<<8) | SSP0_DR;
    }
    
    id = rxDat;   
  
    printf( "0x1F480000 is expected\n" );                 //                                            -> Atmel AT25DF641 Chapter 12.2, Table 12-1 & 12-2
    printf( "0x%08X is coded AT25DF641 ID\n", id );       // Manufacturer and Device ID of Atmel AT35DF641 flash memory
 
    i = id & 0xff;
	ssp_send_dummy_bytes(i);
    while ( 0 < i-- ){                           
		WAIT_FOR_FIFO_RNE
        rxDat = SSP0_DR;                                 
        printf( "%02X ", rxDat );
    }
    printf( "\n" );

	end_operation();
	
    printf( "0x%02X JEDEC Manufacturer ID\n",                     ( (id>>24)&0xFF ) );
    printf( "0x%02X Family Code\n",                               ( (id>>21)&0x07 ) );
    printf( "0x%02X Density Code\n",                              ( (id>>16)&0x1F ) );
    printf( "0x%02X Sub Code\n",                                  ( (id>>13)&0x07 ) );
    printf( "0x%02X Product Version Code\n",                      ( (id>>8) &0x1F ) );
    printf( "0x%02X Extended Device Information String Length\n", (  id     &0xFF ) );    
    

}


__inline void ssp_send_dummy_bytes(unsigned int nbytes) {
	
	int i = 0;
	
	for(i = 0; i < nbytes; i++ ) {
		WAIT_FOR_FIFO_TNF
		SSP0_DR = 0x00;
	}
}

__inline void ssp_read_dummy_bytes(unsigned int nbytes) {

	unsigned int rxData;
	unsigned int i = 0;
	for(i = 0; i < nbytes; i++) {
		WAIT_FOR_FIFO_RNE
		rxData = SSP0_DR;
	}
}


__inline void start_operation(char opcode) {
	
	unsigned int rxData;
    GPIO0_IOCLR = (1<<16);   	
	WAIT_FOR_FIFO_TNF
	SSP0_DR = opcode;          
	WAIT_FOR_FIFO_RNE
	rxData = SSP0_DR;
}
__inline void send_address(unsigned int address) {
	
	SSP0_DR = ((0xff0000 & address) >> 16); 
	
    SSP0_DR = ((0x00ff00 & address) >> 8); 	
	
    SSP0_DR = (0x0000ff & address);
	WAIT_FOR_FIFO_RNE
	ssp_read_dummy_bytes(3);	
}
__inline void end_operation() {
	GPIO0_IOSET = (1<<16);                               
}
__inline void spi_write_enable() {
	start_operation(OPCODE_WRITE_ENABLE);
	end_operation();
}
__inline void spi_global_unprotect() {
	start_operation(OPCODE_WRITE_STATUS_REG1);
	WAIT_FOR_FIFO_TNF
	SSP0_DR = 0;
	ssp_read_dummy_bytes(1);
	end_operation();	
}
void spi_read_data(int addr, int nbytes, unsigned char *data) {
	int i = 0;
	start_operation(OPCODE_READ);	
	send_address(addr);
	ssp_send_dummy_bytes(2);
	ssp_read_dummy_bytes(2);

	for(i = 0; i < nbytes; i++) {
		ssp_send_dummy_bytes(1);
		WAIT_FOR_FIFO_RNE
		data[i] = SSP0_DR;
	}
	end_operation();
}

void spi_write_data(int addr, int nbytes, unsigned char *data) {
	unsigned int i = 0;
	spi_write_enable();
	spi_global_unprotect();
	spi_write_enable();
	start_operation(OPCODE_WRITE);
	send_address(addr);
	for(i = 0; i < nbytes;  i++){
		WAIT_FOR_FIFO_TNF
		SSP0_DR = data[i];
		ssp_read_dummy_bytes(1);
	}
	end_operation();
}

void spi_erase_block(int addr, int block_size_select) {
	char opcode;
	switch(block_size_select){
		case 0:
			opcode = OPCODE_ERASE4;
			break;
		case 1:
			opcode = OPCODE_ERASE32;
			break;
		case 2:
			opcode = OPCODE_ERASE64;
			break;
		default:
			opcode = OPCODE_ERASE4;
	}
	spi_write_enable();
	spi_global_unprotect();
	spi_write_enable();
	
	start_operation(opcode);
	send_address(addr);
	end_operation();	
}

void spi_erase_consecutive_blocks(int addr, int block_size_select, int nblocks) {
    int b_size ;
  
    switch (block_size_select) {
        case 0:
            b_size = B4K_BYTES;
            break;
        case 1:
            b_size = B32K_BYTES;
            break;
        case 2:
            b_size = B64K_BYTES;
            break;
		default:
			b_size = B4K_BYTES;
    }
      
    int i;
    for (i=0; i<nblocks; i++) {
        spi_erase_block(addr, block_size_select);
        spi_wait_ready(); 
        addr += b_size;
    }
} 

void spi_erase_bytes(int addr, int bytes) {
    int n_b64kb, n_b32kb, n_b4kb;
	
	int n_32b = 0;
	int eaddr = addr+bytes;
	while(((addr % B32K_BYTES) > B4K_BYTES) && (addr<eaddr )) {
		 spi_erase_consecutive_blocks(addr, 0, 1);
		 addr += B4K_BYTES;
		 bytes -= B4K_BYTES;
	}	
	if((bytes >= B32K_BYTES) && ((addr % B64K_BYTES) < B4K_BYTES)) {
		 spi_erase_consecutive_blocks(addr, 1, 1);
		 addr += B32K_BYTES;
		 bytes -= B32K_BYTES;
	}
	
	n_b64kb = bytes / B64K_BYTES;
    bytes = bytes % B64K_BYTES;
	
    n_b32kb = bytes / B32K_BYTES;
    bytes = bytes % B32K_BYTES;
      
    n_b4kb = bytes / B4K_BYTES;
    bytes = bytes % B4K_BYTES;   
  
  
    if( bytes > 0){
        n_b4kb +=1;
    }
  
    spi_erase_consecutive_blocks(addr, 2, n_b64kb);
    addr += B64K_BYTES * n_b64kb;
    spi_erase_consecutive_blocks(addr, 1, n_b32kb);
    addr += B32K_BYTES * n_b32kb;  
    spi_erase_consecutive_blocks(addr, 0, n_b4kb);    
} 

void spi_write_bytes(int addr, int nbytes, unsigned char *data) {
	
	int firstpage = 0;
	int inpage = 0;
	int next_page_start = 0;
	int newsize = 0;
	int this_page = 0;
	if(addr % 256) {
	this_page = (addr / 256);
	next_page_start = this_page + 1;
	next_page_start *= 256;
	newsize = next_page_start - addr;	
	if(newsize > nbytes) newsize = nbytes;
	printf("writing %d bytes tom 0x%x until next 256b block\n", newsize, next_page_start); 
	spi_write_data(addr, newsize, data);
	spi_wait_ready();	
 	data += newsize;
	nbytes -= newsize;	
	addr = next_page_start;

	}
	while(nbytes > 256) {
	printf("writing 256 bytes to 0x%x. %d remaining\n", addr, nbytes);
	spi_write_data(addr, 256, data);
	addr += 256;
	data += 256;
	nbytes -= 256;
	spi_wait_ready();
	} 

	if(nbytes > 0) {
		printf("%d bytes left, writing to 0x%x\n", nbytes, addr);
		spi_write_data(addr, nbytes, data);
		spi_wait_ready();
	}
}


void spi_erase_all() {
	spi_write_enable();
	start_operation(OPCODE_CHIP_ERASE);
	end_operation();
}

void spi_wait_ready() {
	int status = 0;
	start_operation(OPCODE_READ_STATUS_REG);
	do {
		ssp_send_dummy_bytes(2);
		WAIT_FOR_FIFO_RNE
		status = SSP0_DR;
		WAIT_FOR_FIFO_RNE
		status = SSP0_DR;
	} while (status & STATUS_READY);
	
	end_operation();	
}
